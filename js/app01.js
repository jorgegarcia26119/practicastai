function hacerPeticon(){
    const http = new XMLHttpRequest;
    const url ="https://jsonplaceholder.typicode.com/albums";
    //validar la respuesta 
    
    http.onreadystatechange = function(){

        if(this.status == 200 && this.readyState == 4){
            // aqui se dibuja la pagina
            let res = document.getElementById("lista");
            const json = JSON.parse(this.responseText);

            //ciclo para ir tomando cada uno de los regustros

            for(const datos of json){

                res.innerHTML += '<tr> <td class ="columna1">'+ datos.userID + '</td>'
                    + '<td class ="columna2">'+ datos.id + '</td>'
                    + '<td class ="columna3">'+ datos.title + '</td>'
            }
            res.innerHTML += "</tbody>" 
        }
    }
    http.open('GET', url,true);
    http.send();
}

// codificar los botones

document.getElementById("btnCargar").addEventListener("click",function(){
    hacerPeticon();

})

document.getElementById("btnLimpiar").addEventListener("click",function(){
    let res= document.getElementById("lista");
    res.innerHTML="";
    
})
