//practica 02 uso del objeto fetch
//maejando promesas
//manejando await


//usando Promesas
llamandoFetch =()=>{
const url="https://jsonplaceholder.typicode.com/todos";
fetch(url)
.then(respuesta => respuesta.json())
.then(data => mostrarTodos(data))
.catch((reject)=>{
    console.log("surgio un error"+ reject);
    
});
}
//usando await

const llamandoAwait = async ()=>{
    try{
        
        const url="https://jsonplaceholder.typicode.com/todos"
        const respuesta = await fetch(url)
        const data = await respuesta.json()
        mostrarTodos(data);

    }
    catch(error){
        console.log("Surgio un error"+ error);
    }

}

const mostrarTodos=(data)=>{
    console.log(data);

    const res= document.getElementById('respuesta');
    res.innerHTML="";
    for(const item of data){

        res.innerHTML += '<tr> <td class ="columna1">'+ item.userId + '</td>'
            + '<td class ="columna2">'+ item.id + '</td>'
            + '<td class ="columna3">'+ item.title + '</td>'
            + '<td class ="columna4">'+ item.completed + '</td>'
    }
    res.innerHTML += "</tbody>" 

    for(let item of data){

        res.innerHTML += item.userId + ", " + item.id +","+ item.title + ","+ item.completed + '<br>'
    }
    
}

//codificar lo botones 
document.getElementById("btnCargarP").addEventListener('click', function(){
    llamandoFetch();
});

document.getElementById("btnCargarA").addEventListener('click', function(){
    llamandoAwait();
});

document.getElementById("btnLimpiar").addEventListener('click', function(){
    const res= document.getElementById('respuesta');
    res.innerHTML="";
});