function hacerPeticion(){
    const http=new XMLHttpRequest;
    const url="https://jsonplaceholder.typicode.com/users";
    //Validar la respuesta
    http.onreadystatechange=function(){
        if(this.status==200 && this.readyState==4){
            //aqui se dibuja la pagina
            let res=document.getElementById("lista");
            const json = JSON.parse(this.responseText);

            //CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTROS

            for(const datos of json){
                res.innerHTML += '<tr> <td class ="columna1">' + datos.id + '</td>'
                + '<td class ="columna2">' + datos.name+ '</td>'                
                + '<td class ="columna3">' + datos.username+ '</td>'
                + '<td class ="columna4">' + datos.email + '</td>' 
                + '<td class ="columna5">' + datos.address.street + '</td>' 
                + '<td class ="columna6">' + datos.address.suite + '</td>' 
                + '<td class ="columna7">' + datos.address.city + '</td>'
                + '<td class ="columna8">' + datos.address.zipcode + '</td>' 
                + '<td class ="columna9">' + datos.phone + '</td>' 
                + '<td class ="columna10">' + datos.website+ '</td>' 
                + '<td class ="columna11">' + datos.company.name + '</td> </tr>'

            } 
            res.innerHTML +="</tbody>"
        }
    }
    http.open('GET',url,true);
    http.send();
}
//codificar los botones

document.getElementById("btnCargar").addEventListener("click",function(){
    hacerPeticion();
});
document.getElementById("btnLimpiar").addEventListener("click",function(){
    let res=document.getElementById("lista");
    res.innerHTML="";
})