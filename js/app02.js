function hacerPeticion() {
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/albums";
    let idE = 0;
    //Validar la respuesta
    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            //aqui se dibuja la pagina 
            let res = document.getElementById("idB");
            const json = JSON.parse(this.responseText);

            //CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTROS
            for (const datos of json) {
                if (res.value == datos.id.toString()) {
                    let user = document.getElementById("user");
                    user.value= datos.userId;
                    let title = document.getElementById("title");
                    title.value= datos.title;
                    idE = 1; 
                    break;
                }
            }
            if (idE != 1) {
                alert("");
                idE = 0;
                limpiar();
            }
        }
    }
    http.open('GET', url, true);
    http.send();
}
function limpiar() {
    let title = document.getElementById("title");
    let id = document.getElementById("idB");
    let user = document.getElementById("user");
    user.value="";
    title.value = "";
    id.value = "";
}
document.getElementById("btnBuscar").addEventListener("click", function () {
    hacerPeticion();
});
document.getElementById("btnLimpiar").addEventListener("click", function () {
    limpiar();
});